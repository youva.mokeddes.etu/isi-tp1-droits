# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Youva MOKEDDES youva.mokeddes.etu@univ-lille.fr

- Nom, Prénom, email: Ferhat HOCINE ferhat.hocine.etu@univ-lille.fr

## Question 1
creation de l'utilisateur et le fichier:
	sudo useradd toto
	sudo adduser toto ubuntu
	touch titi.txt
	sudo chown toto titi.txt
enlever les permission :
	sudo chmod -w titi.txt
	résultat(chmod: titi.txt: new permissions are r--rw-r--, not r--r--r--)
lancer:sudo -u toto vim titi.txt

le processus ne peut pas écrire dans titi.txt car il ne déspose pas des permission écriture(r--rw-r--) que on lui a enlevé avec la commande (sudo chmod -w titi.txt)

## Question 2

(x) signifie l'accés  pour un répertoire
création de dossier et enlever les permission:
	mkdir mydir
	chmod g-x mydir   //enlever les permission d'accès pour le groupe
basculer vers toto:
	sudo -u toto -s
	cd mydir
	résultat(Permission denied)
	
ayant enlevé le droit d'accès au répertoire mydir pour le groupe et sachant que toto fait parti de se groupe toto n'est donc pas autorisé à accèdé à ce répertoire.

2-2)
retour à ubuntu et création du fichier:
	suspend
	cd mydir
	touch data.txt
on repasse à toto:
	cd ..
	sudo -u toto -s
	ls -al mydir
résultat :
	ls: cannot access 'mydir1/.': Permission denied
	ls: cannot access 'mydir1/..': Permission denied
	ls: cannot access 'mydir1/data.txt': Permission denied
	total 0
	d????????? ? ? ? ?            ? .
	d????????? ? ? ? ?            ? ..
	-????????? ? ? ? ?            ? data.txt

on arrive juste à voir le contenu du dossier mais sans les permissions.


## Question 3
//
ubuntu@instance1:~$ ./a.out mydir/data.txt
RUID : 1000 
RGID : 1000 
EUID : 1000 
EGID : 1000 
//
toto@instance1:/home/ubuntu$ ./a.out mydir/data.txt
euid: 1001
egid: 1001
ruid: 1001
rgid: 1001
Segmentation fault (core dumped)
//
ubuntu@instance1:~$ chmod u+s a.out

toto@instance1:/home/ubuntu$ ./a.out mydir/data.txt
euid: 1000
egid: 1001
ruid: 1001
rgid: 1001

oui le fichier a été ouvert en lecture

## Question 4

ubuntu@instance1:~$ ll suid.py
-rw-rw-r-- 1 ubuntu ubuntu 340 Feb  1 12:47 suid.py

--

ubuntu@instance1:~$ python3 suid.py mydir/data.txt
Egid = 1000
Euid=  1000
opened correctly


--

toto@instance1:/home/ubuntu$ python3 suid.py mydir/data.txt
Egid = 1001
Euid=  1001
Failed to open file




## Question 5

La commande chfn permet de modifier les informations relatives aux utilisateurs (Nom, Numéro de téléphone...)
--
ubuntu@instance1:~$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
explication: seul root a la permission d'apporter des modification à ces informations.

-- 
toto@instance1:/home/ubuntu$ chfn
Password: 
Changing the user information for toto
Enter the new value, or press ENTER for the default
	Full Name: 
	Room Number []: 1
	Work Phone []: 0711111111
	Home Phone []: 0707070707

]
--

les information on été mises à jour 
toto:x:1004:1004:,1,0711111111,0707070707:/home/toto:/bin/sh

## Question 6

les mots de passe sont stockés dans le fichier shadow su répertoire etc, seul l'utilisateur root a la permission lecture et écriture de ce fichier 
les utilisateur du groupe shadow ont seulement la permission de lecture de ce fichier
les mots de passe sont crypty dans le fichier shadow et cela afin que personne ne puisse accéder au mots de passe des autres utilisateur 


# Question 7

users:
sudo adduser lambda_a
sudo adduser lambda_b
sudo adduser adminn
groupes:
sudo groupadd groupe_a
sudo groupadd groupe_b
sudo groupadd groupe_c

add user to groupe:
sudo adduser lambda_a groupe_a
sudo adduser lambda_b groupe_b
sudo adduser lambda_a groupe_c
sudo adduser lambda_b groupe_c
sudo adduser adminn groupe_a
sudo adduser adminn groupe_b
sudo adduser adminn groupe_c

dossier:
mkdir dir_a
mkdir dir_b
mkdir dir_c

propriétaire:
sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b
sudo chgrp groupe_c dir_c

droits:
sudo chmod o-rw dir_a
sudo chmod o-rw dir_b
sudo chmod g-w dir_c
sudo chmod +t dir_a // que le propritaire qui peut renommer ou effacer le contenu d'un fichier racine de dir_a
sudo chmod +t dir_b
sudo chmod g+s dir_a //groupe lecture
sudo chmod g+s dir_b

sudo chown adminn:groupe_a dir_a // pour que adminn puisse renommer ou effacer le contenu d'un fichier racine de dir_a
sudo chown adminn:groupe_b dir_b
sudo chown adminn:groupe_c dir_c




## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








