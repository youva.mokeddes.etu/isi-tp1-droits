#!/bin/bash



echo "*******************"
echo "-------Test0-------"
echo "*******************"
echo "lecture de fichier dans les répertoire"
echo "dir_b ls "
ls -al dir_b

echo "*******************"
echo "-------Test1-------"
echo "*******************"

echo "creation contenu dans dir_B"
cd dir_b
echo "dir_B  avant création "
ls -al
mkdir dossierTest1B
touch fichierTest1B.txt
echo "dir_B  après création "
ls -al
rm fichierTest1B.txt
echo "dir_B  après suppression du fichier crée par le même utilisateur "
ls -al


echo "*******************"
echo "-------Test2-------"
echo "*******************"
echo "teste que on ne peut pas supprimer un fichier ou dossier créer par un autre utilisateur"
echo "la création du fichier est faites dans le script adminn"
echo "avant"
ls -al
rm adminCreationForLambdaBTest.txt
echo "Après"
ls -al

echo "*******************"
echo "-------Test3-------"
echo "*******************"
echo "ne peux pas accèder au fichier de dir_A"
cd ..
ls -al
ls -al dir_a

echo "fin du Teste Lambda_b"
echo ""

