#!/bin/bash



echo "*******************"
echo "-------Test0-------"
echo "*******************"
echo "lecture de fichier dans les répertoire"
echo "dir_a ls "
ls -al dir_a

echo "*******************"
echo "-------Test1-------"
echo "*******************"

echo "creation contenu dans dir_a"
cd dir_a
echo "dir_a  avant création "
ls -al
mkdir dossierTest1A
touch fichierTest1A.txt
echo "dir_a  après création "
ls -al
rm fichierTest1A.txt
echo "dir_a  après suppression du fichier crée par le même utilisateur "
ls -al


echo "*******************"
echo "-------Test2-------"
echo "*******************"
echo "teste que on ne peut pas supprimer un fichier ou dossier créer par un autre utilisateur"
echo "la création du fichier est faites dans le script adminn"
echo "avant"
ls -al
rm adminCreationForLambdaATest.txt
echo "Après"
ls -al

echo "*******************"
echo "-------Test3-------"
echo "*******************"
echo "ne peux pas accèder au fichier de dir_b"
cd ..
ls -al
ls -al dir_b

echo "fin du Teste Lambda_a"
echo ""

