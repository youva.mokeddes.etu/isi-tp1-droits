#!/bin/bash


echo "*******************"
echo "-------Test0-------"
echo "*******************"
echo "lecture de fichier dans les répertoire"
echo "dir_a ls "
ls -al dir_a
echo "dir_b ls "
ls -al dir_b
echo "dir_c ls "
ls -al dir_c

echo "*******************"
echo "-------Test1-------"
echo "*******************"

echo "modification contenu dir_a"
cd dir_a
echo "dir_a  avat création "
ls -al
mkdir dossierTestA
touch fichierTestA.txt
touch adminCreationForLambdaATest.txt

echo "dir_a  après création "
ls -al
rm fichierTestA.txt
echo "dir_a  après suppression du fichier crée par le même utilisateur "
ls -al

echo "*******************"
echo "-------Test2-------"
echo "*******************"

echo "modification contenu dir_b"
cd ../dir_b
echo "dir_b  avant création "
ls -al
mkdir dossierTestB
touch fichierTestB.txt
touch adminCreationForLambdaBTest.txt

echo "dir_b  après création "
ls -al
rm fichierTestB.txt
echo "dir_b  après suppression "
ls -al

echo "*******************"
echo "-------Test3-------"
echo "*******************"
echo "modification contenu dir_c"
cd ../dir_c
echo "dir_c  avant création "
ls -al
mkdir dossierTestC
touch fichierTestC.txt
touch adminCreationForLambdaCTest.txt
echo "dir_c  après création "
ls -al
rm fichierTestC.txt
echo "dir_c  après suppression "
ls -al

echo "fin du Teste adminn"
echo ""
