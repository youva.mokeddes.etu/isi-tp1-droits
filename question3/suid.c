#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	FILE *file;
	file = fopen(argv[1], "r");
	
    printf("euid: %u\n", geteuid());
    printf("egid: %u\n", getegid());
    printf("ruid: %u\n", getuid());
    printf("rgid: %u\n", getgid());
    
    char chaine;
    chaine = fgetc(file); 
    while (chaine != EOF) 
    { 
        printf ("%c", chaine); 
        chaine = fgetc(file); 
    } 
    fclose(file);
    exit(EXIT_SUCCESS);    
}

